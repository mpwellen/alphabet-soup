# -*- coding: utf-8 -*-
"""

@author: mpwel
"""
# -*- coding: utf-8 -*-

# This function returns the values from the file

def parseInput(fileName):
    if(fileName == ""):
        fileName = "testInputB.txt"
    f = open(fileName, "r")
    testGrid = f.readlines()

    numberLines = len(testGrid)

    inputDims = list(testGrid[0])
    inputDimX = inputDims[0]
    inputDimY = inputDims[2]

    testGridArr = []
    j = 1
    while(j <= int(inputDimY)):
        testGridArr.append(testGrid[j].replace(' ', ''))
        j = j + 1
        
    testValues = []
    while(j < numberLines):
        testValues.append(testGrid[j])
        j = j + 1
    return(testGridArr, testValues, inputDimX, inputDimY)

# Search for horizontal matches
def searchRight(soughtString):
    #Start in top right.
    x1 = 0
    y1 = 0
    found = True
    soughtList = list(soughtString)
    lengthInput = len(soughtList)
    # Loop through y-axis
    while(y1 < int(inputDimY)):
        x1 = 0
        # Loop through x-axis
        while(x1 < int(inputDimX) ):
            tempLoc = 0
            x2 = x1
            testLength = 0
            found = True
            while(found == True):
                if(testGridArr[y1][x2] == soughtList[tempLoc]):
                    x2 += 1
                    testLength += 1
                    tempLoc += 1
                    if(testLength == lengthInput):
                        location = [x1,y1,x1+lengthInput - 1,y1]
                        return location
                else:
                    found = False
            x1 += 1
        y1 += 1
    return None
        
# Search for vertical matches
def searchDown(soughtString):
    x1 = 0
    y1 = 0
    found = True
    soughtArray = list(soughtString)
    lengthInput = len(soughtArray)
    while(y1 < int(inputDimY)):
        x1 = 0
        while(x1 < int(inputDimX)):
            tempLoc = 0
            y2 = y1
            testLength = 0
            found = True
            while(found == True and y2 < int(inputDimY)):
                if(testGridArr[y2][x1] == soughtArray[tempLoc]):
                    y2 += 1
                    testLength += 1
                    tempLoc += 1
                    if(testLength == lengthInput):
                        location = [x1,y1,x1,y1+lengthInput - 1]
                        return location
                else:
                    found = False
            x1 += 1
        y1 += 1
    return None

# Search for diagonal matches.
def searchDiagonal(soughtString):
    x1 = 0
    y1 = 0
    found = True
    soughtArray = list(soughtString)
    lengthInput = len(soughtArray)
    while(y1 < int(inputDimY)):
        x1 = 0
        while(x1 < int(inputDimX)):
            tempLoc = 0
            y2 = y1
            x2 = x1
            testLength = 0
            found = True
            while(found == True and x2 < int(inputDimX) and y2 < int(inputDimY)):
                if(testGridArr[y2][x2] == soughtArray[tempLoc]):
                    y2 += 1
                    x2 += 1
                    testLength += 1
                    tempLoc += 1
                    if(testLength == lengthInput):
                        location = [x1,y1,x1+lengthInput - 1,y1+lengthInput - 1]
                        return location
                else:
                    found = False
            x1 += 1
        y1 += 1
    return

# Reverse sought word direction to re-use above functions
def reverseWord(originalString):
    reversedString = ""
    for char in originalString[::-1]:
        reversedString += char
    return reversedString

# Print result in original word
def printResultsNormal(x, foundWord):
    print(str(x).strip() + " "+ str(foundWord[0])+':'+str(foundWord[1])+ ' ' + str(foundWord[2]) + ':' + str(foundWord[3]))

# Print result for reversed word
def printResultsReverse(x, foundWord):
    print(str(x).strip() + " "+ str(foundWord[2])+':'+str(foundWord[3])+ ' ' + str(foundWord[0]) + ':' + str(foundWord[1]))

def runTest():
    for x in testValues:
        x = x.strip()
        reverse = reverseWord(x)
        if(searchRight(x) != None):
            foundWord = searchRight(x)
            printResultsNormal(x, foundWord)
        elif(searchRight(reverse) != None):
            foundWord = searchRight(reverse)
            printResultsReverse(x, foundWord)
        elif(searchDown(x) != None):
            foundWord = searchDown(x) 
            printResultsNormal(x, foundWord)
        elif(searchDown(reverse) != None):
            foundWord = searchDown(reverse) 
            printResultsReverse(x, foundWord)    
        elif(searchDiagonal(x) != None):
            foundWord = searchDiagonal(x) 
            printResultsNormal(x, foundWord)
        elif(searchDiagonal(reverse) != None):
            foundWord = searchDiagonal(reverse) 
            printResultsReverse(x, foundWord)
        else: 
            print("Word ", x, " not found")
            
fileName = input('What is your file name?\n') 
testGridArr, testValues, inputDimX, inputDimY = parseInput(fileName)

runTest()