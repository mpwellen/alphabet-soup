const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);

dataInput = [];
dims = [];
searchArr = [];
function getFile() {
	return readFile('test.txt', "utf8");
}

async function searchRight(soughtWord, direction) {
	soughtList = soughtWord;
	let dimX = dims[0]
	let dimY = dims[2]
	let x1 = 0;
	let y1 = 0;
	let found = true;
	lengthInput = soughtList.length;
	while(y1 < dimY){
		x1 = 0;
		while(x1 < dimX){
			let tempLoc = 0;
			let x2 = x1;
			let testLength = 0;
			found = true;
			while(found == true){
				if(x2 < dimX && searchArr[y1][x2] == soughtList[tempLoc]) {
					x2 += 1;
					testLength += 1;
					tempLoc += 1;
					if(testLength == lengthInput) {
						if(direction == 'f') {
							let result = soughtWord+" "+ x1+':'+y1+" "+(x1+lengthInput-1)+':'+y1;
							console.log(result.trim())
						}
						else if(direction == 'r') {
							let result = soughtWord.split('').reverse().join('')+" "+(x1+lengthInput-1)+ ':'+y1 + " "+x1+ ':'+y1;
							console.log(result.trim())
						}
						return;
					}
				}
				else { 
					found = false;
				}
			}
		x1 += 1;
		}
	y1 += 1;
	}
		return 'X';

}

async function searchDown(soughtWord, direction) {
        let x1 = 0;
        let y1 = 0;
	let dimX = dims[0];
	let dimY = dims[2];
        let found = true;
        soughtList = soughtWord;
        lengthInput = soughtList.length;
        while(y1 < dimY){
                x1 = 0;
                while(x1 < dimX){
                        let tempLoc = 0;
                        let y2 = y1;
                        let testLength = 0;
                        found = true;
                        while(found == true){
                                if(y2 < dimY && searchArr[y2][x1] == soughtList[tempLoc]){
					y2 += 1;
                                        testLength += 1;
                                        tempLoc += 1;
                                        if(testLength == lengthInput){
						if(direction == 'f') {
							let result = soughtWord+" "+ x1+':'+y1+" "+x1+':'+(y1+lengthInput-1);
                                                	console.log(result.trim());
						}
						else if(direction == 'r') {
							let result = soughtWord.split('').reverse().join('')+ x1+':' + (y1+lengthInput-1)+ "" +x1+ ':'+y1;
							console.log(result.trim());
						}
                                                return;
                                        } 
                                }
                                else {
                                        found = false;
                                }
                        }
                x1 += 1;
                }
        y1 += 1;
        }
                return 'X';

}

async function searchDiagonal(soughtWord, direction) {
        soughtList = soughtWord;
        let x1 = 0;
        let y1 = 0;
        let found = true;
	let dimX = dims[0];
	let dimY = dims[2];
        lengthInput = soughtList.length;
        while(y1 < dimY){
                x1 = 0;
                while(x1 < dimX){
                        let tempLoc = 0;
			let x2 = x1;
                        let y2 = y1;
                        let testLength = 0;
                        found = true;
                        while(found == true){
                                if(y2 < dimY && x2 < dimX && searchArr[y2][x2] == soughtList[tempLoc]){
                                        y2 += 1;
					x2 += 1;
                                        testLength += 1;
                                        tempLoc += 1;
                                        if(testLength == lengthInput){
						if(direction == 'f') {
                                                	let result = soughtWord+" "+x1+':'+y1+" "+ (x1+lengthInput-1)+':'+(y1+lengthInput-1);
							console.log(result.trim());
						}
						else if(direction == 'r') {
							let result = soughtWord.reverse().join('')+ " " + (x1+lengthInput-1)+ ':'+(y1+lengthInput -1)+ x1 +':'+y1;
							console.log(result.trim());
						}
						return;
                                        }
                                }
                                else {
                                        found = false;
                                }
                        }
                x1 += 1;
                }
        y1 += 1;
        }
                return 'X';

}

async function parseData() { 
	let testArr = dataInput.split('\n');
	dims = testArr[0];
	let i;
	for(i = 1; i<=dims[2]; i++){
		searchArr.push(testArr[i].split(' '));
	}
	let searchTerms = [];
	let j = i;
	while(j < testArr.length) {
		searchTerms.push(testArr[j]);
		j++;
	}
	let k = 0;
	while(k < searchTerms.length) {	
		let reverse = searchTerms[k].split('').reverse().join('');
		let answer;
		answer = searchRight(searchTerms[k], 'f');
		answer = searchRight(reverse, 'r');
		answer = searchDown(searchTerms[k], 'f');
		answer = searchDown(reverse, 'r');
		answer = searchDiagonal(searchTerms[k], 'f');
		answer = searchDiagonal(reverse, 'r');
		k += 1;
	}
}

async function runTest(data) {
	dataInput = data;
	parseData();
}

getFile().then(data => {
	runTest(data);
})
